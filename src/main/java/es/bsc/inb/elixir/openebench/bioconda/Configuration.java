/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.bioconda;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public final class Configuration {
    
    public final static String GIT;
    public final static String BRANCH;
    public final static String PATH;
    public final static String USER;
    public final static String PASSWORD;
    
    static {
        String git = null;
        String branch = null;
        String path = null;
        String user = null;
        String password = null;
        try (InputStream in = Configuration.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
            final Properties p = new Properties();
            p.load(in);
            git = p.getProperty("biotools.git");
            branch = p.getProperty("biotools.branch", "origin/master");
            path = p.getProperty("biotools.path");
            user = p.getProperty("openebench.user");
            password = p.getProperty("openebench.password");
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        GIT = git;
        BRANCH = branch;
        PATH = path;
        USER = user;
        PASSWORD = password;
    }
}
