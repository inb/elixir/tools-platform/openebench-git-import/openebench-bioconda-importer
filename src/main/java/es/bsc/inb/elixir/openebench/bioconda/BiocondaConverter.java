/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.bioconda;

import es.bsc.inb.elixir.biotools.model.LicenseType;
import es.bsc.inb.elixir.openebench.model.tools.Tool;
import es.bsc.inb.elixir.openebench.model.tools.ToolType;
import es.bsc.inb.elixir.openebench.model.tools.Web;
import es.bsc.inb.elixir.openebench.repository.OpenEBenchEndpoint;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public class BiocondaConverter {
   
    public static Tool convert(Map<String, Object> attributes) {
        
        String id = null;
        List<String> altIDs = null;

        final Object name = attributes.get("name");
        
        final Object biotools_id = attributes.get("biotools_id");
        if (biotools_id instanceof String) {
            id = biotools_id.toString();
        }

        final Object identifiers = attributes.get("identifiers");
        if (identifiers instanceof String) {
            if (identifiers.toString().startsWith("biotools:")) {
                id = identifiers.toString().substring("biotools:".length());
            }
        } else if (identifiers instanceof List) {
            final List list = (List)identifiers;
            altIDs = new ArrayList<>(list.size());
            for (Object o : list) {
                final String altID = o.toString();
                if (altID.startsWith("biotools:")) {
                    if (id == null) {
                        id = altID.substring("biotools:".length());
                    } else if (!id.equals(altID.substring("biotools:".length()))) {
                        Logger.getLogger(BiocondaConverter.class.getName()).log(Level.INFO, "incogerent biotools identifiers: {0} {1}", new Object[] {id, altID});
                    }
                } else {
                    altIDs.add(altID);
                }
            }
        }
        
        if (id == null && name instanceof String) {
            if (name.toString().startsWith("bioconductor-")) {
                id = name.toString().substring("bioconductor-".length()).toLowerCase().trim();
            } else if (name.toString().startsWith("r-")) {
                id = name.toString().substring("r-".length()).toLowerCase().trim();
            } else {
                id = name.toString().toLowerCase().trim();
            }
        }
        
        StringBuilder idTemplate = new StringBuilder(OpenEBenchEndpoint.TOOL_URI_BASE).append("bioconda:").append(id);
        
        final Object version = attributes.get("version");
        if (version instanceof String) {
            if (version != null) {
                idTemplate.append(':').append(version.toString().replace(' ', '_'));
            }
            idTemplate.append('/').append(ToolType.WORKFLOW.type);
        }
        
        Web web = null;
        final Object home = attributes.get("home");
        if (home instanceof String) {
            web = new Web();
            try {
                URI homepage = URI.create(home.toString());
                web.setHomepage(homepage);
                idTemplate.append('/').append(homepage.getHost());
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(BiocondaConverter.class.getName()).log(Level.INFO, "invalid homepage: {0}", home);
            }
        }
        
        Tool tool = new Tool(URI.create(idTemplate.toString()), ToolType.WORKFLOW.type);

        tool.setWeb(web);
        tool.setAlternativeIDs(altIDs);

        if (name instanceof String) {
            tool.setName(name.toString());
        }
        
        final Object description = attributes.get("description");
        if (description instanceof String) {
            tool.setDescription(description.toString());
        }
        
        final Object summary = attributes.get("summary");
        if (summary instanceof String) {
            tool.setShortDescription(summary.toString());
        }
        
        final Object license = attributes.get("license");
        if (license instanceof String) {
            try {
                LicenseType.fromValue(license.toString()); // just to check if valid.
            } catch(IllegalArgumentException ex) {
                Logger.getLogger(BiocondaConverter.class.getName()).log(Level.INFO, "not biotools license: {0}", license);
            }
            tool.setLicense(license.toString());
        }

        return tool;
    }
    
}
