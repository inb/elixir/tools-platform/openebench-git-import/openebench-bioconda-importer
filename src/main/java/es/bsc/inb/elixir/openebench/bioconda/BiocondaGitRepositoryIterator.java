/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.bioconda;

import com.google.common.jimfs.Jimfs;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.util.SystemReader;
import org.yaml.snakeyaml.Yaml;

/**
 * @author Dmitry Repchevsky
 */

public class BiocondaGitRepositoryIterator 
        implements Iterator<Map<String, Object>>, Closeable {

    private DirectoryStream<Path> directories;
    private Iterator<Path> iterator;
    private Path file_path;
    
    public BiocondaGitRepositoryIterator(
            final String git, 
            final String branch, 
            final String path) throws IOException {

        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);
        
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        com.google.common.jimfs.Configuration config = 
                com.google.common.jimfs.Configuration.unix().toBuilder()
                        .setWorkingDirectory("/")
                        .build();

        final FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git g = Git.cloneRepository()
                          .setURI(git)
                          .setDirectory(root)
                          .setBranch(local)
                          .setRemote(branch)
                          .call()) {
        } catch (GitAPIException ex) {
            Logger.getLogger(BiocondaGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
            throw new IOException(ex);
        }

        directories = Files.newDirectoryStream(root.resolve(path));
        iterator = directories.iterator();
    }
    
    @Override
    public boolean hasNext() {
        if (file_path != null) {
            return true;
        }
        while (iterator.hasNext()) {
            final Path directory = iterator.next();
            final String dir = directory.getFileName().toString();
            final Path path = directory.resolve("bioconda_" + dir + ".yaml");

            if (Files.exists(path)) {
                file_path = path;
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Object> next() {
        Yaml yaml = new Yaml();
        try (Reader reader = Files.newBufferedReader(file_path)) {
            file_path = null;
            return yaml.load(reader);
        } catch (Exception ex) {
            Logger.getLogger(BiocondaGitRepositoryIterator.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        
        return null;
    }

    @Override
    public void close() throws IOException {
        directories.close();
    }
}
